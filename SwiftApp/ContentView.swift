//
//  ContentView.swift
//  SwiftApp
//
//  Created by Brzeski, Piotr on 2020-10-09.
//

import SwiftUI
import SwiftPackage

struct ContentView: View {
	@State var show = false
	
	var body: some View {
		VStack {
			Button(show ? "Hide" : "Show") { show.toggle() }
				.padding(.bottom)
				.padding(.top, 32)
			Divider()
			if show {
				Spacer()
				Image(systemName: imageName())
					.font(Font.system(size: 150).bold())
					.foregroundColor(imageColor())
			}
			Spacer()
		}
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}
